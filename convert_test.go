package main

import (
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

func TestConvert(t *testing.T) {
	in := `{
  "warnings": [
    {
      "warning_type": "Cross-Site Scripting",
      "warning_code": 107,
      "fingerprint": "e0636b950dd005468b5f9a0426ed50936e136f18477ca983cfc51b79e29f6463",
      "check_name": "SanitizeMethods",
      "message": "rails-html-sanitizer 1.0.3 is vulnerable (CVE-2018-3741). Upgrade to 1.0.4",
      "file": "Gemfile.lock",
      "line": 97,
      "link": "https://groups.google.com/d/msg/rubyonrails-security/tP7W3kLc5u4/uDy2Br7xBgAJ",
      "code": null,
      "render_path": null,
      "location": null,
      "user_input": null,
      "confidence": "Medium",
      "cwe_id": [79]
    },
    {
      "warning_type": "Command Injection",
      "warning_code": 14,
      "fingerprint": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
      "check_name": "Execute",
      "message": "Possible command injection",
      "file": "app/controllers/application_controller.rb",
      "line": 5,
      "link": "https://groups.google.com/d/msg/rubyonrails-security/tP7W3kLc5u4/uDy2Br7xBgAJ",
      "code": "system(params[:cmd])",
      "render_path": null,
      "location": {
        "type": "method",
        "class": "ApplicationController",
        "method": "foo"
      },
      "user_input": "params[:cmd]",
      "confidence": "High",
      "cwe_id": [77]
    },
    {
      "warning_type": "SQL Injection",
      "warning_code": 0,
      "fingerprint": "a16b9f9e2342465933a444ffdc01de6dc51d10eeb27de907f341a3318a6e1069",
      "check_name": "SQL",
      "message": "Possible SQL injection (CVE-2018-1234)",
      "file": "app/models/application_record.rb",
      "line": 8,
      "link": "https://brakemanscanner.org/docs/warning_types/sql_injection/",
      "code": "where(\"#{user_input}\")",
      "render_path": null,
      "location": {
        "type": "method",
        "class": "ApplicationRecord",
        "method": "some_method"
      },
      "user_input": "user_input",
      "confidence": "Medium",
      "cwe_id": [89]
    }
  ]
}`

	var scanner = metadata.IssueScanner

	r := strings.NewReader(in)
	version := report.Version{
		Major: 15,
		Minor: 0,
		Patch: 7,
	}
	want := &report.Report{
		Version: version,
		Vulnerabilities: []report.Vulnerability{

			{
				Category:   report.CategorySast,
				Scanner:    &scanner,
				Name:       "Possible command injection",
				Message:    "Possible command injection",
				CompareKey: "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
				Confidence: report.ConfidenceLevelHigh,
				Severity:   report.SeverityLevelHigh,
				Location: report.Location{
					Class:     "ApplicationController",
					Method:    "foo",
					File:      "x/app/controllers/application_controller.rb",
					LineStart: 5,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "brakeman_warning_code",
						Name:  "Brakeman Warning Code 14",
						Value: "14",
						URL:   "",
					},
					{
						Type:  "cwe",
						Name:  "CWE-77",
						Value: "77",
						URL:   "https://cwe.mitre.org/data/definitions/77.html",
					},
				},
				Links: []report.Link{
					{
						URL: "https://groups.google.com/d/msg/rubyonrails-security/tP7W3kLc5u4/uDy2Br7xBgAJ",
					},
				},
			},
			{
				Category:   report.CategorySast,
				Scanner:    &scanner,
				Name:       "Possible SQL injection (CVE-2018-1234)",
				Message:    "Possible SQL injection (CVE-2018-1234)",
				CompareKey: "a16b9f9e2342465933a444ffdc01de6dc51d10eeb27de907f341a3318a6e1069",
				Confidence: report.ConfidenceLevelMedium,
				Severity:   report.SeverityLevelMedium,
				Location: report.Location{
					Class:     "ApplicationRecord",
					Method:    "some_method",
					File:      "x/app/models/application_record.rb",
					LineStart: 8,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "brakeman_warning_code",
						Name:  "Brakeman Warning Code 0",
						Value: "0",
						URL:   "https://brakemanscanner.org/docs/warning_types/sql_injection/",
					},
					{
						Type:  "cve",
						Name:  "CVE-2018-1234",
						Value: "CVE-2018-1234",
						URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234",
					},
					{
						Type:  "cwe",
						Name:  "CWE-89",
						Value: "89",
						URL:   "https://cwe.mitre.org/data/definitions/89.html",
					},
				},
				Links: []report.Link{},
			},
		},
		Analyzer: "brakeman",
		Config:   ruleset.Config{Path: ruleset.PathSAST},
	}
	got, err := convert(r, "x")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}

	require.Equal(t, "15.0.7", got.Version.String())
}

func TestIdentifiers(t *testing.T) {
	warnings := []Warning{
		{
			WarningType: "Cross-Site Scripting",
			WarningCode: 43,
			Message:     "Upgrade to Rails 3.0.17. In Rails 3.0.3 `select_tag` is vulnerable (CVE-2012-3463)",
			Confidence:  "high",
			CweID:       []int{79},
		},
		{
			WarningType: "Dangerous Eval",
			WarningCode: 13,
			UserInput:   "high",
			Message:     "User input in eval",
			Confidence:  "high",
			CweID:       []int{913, 95},
		},
	}

	want := [][]report.Identifier{
		{
			report.Identifier{
				Type:  "brakeman_warning_code",
				Name:  "Brakeman Warning Code 43",
				Value: "43",
			},
			report.CVEIdentifier("CVE-2012-3463"),
			report.CWEIdentifier(79),
		},
		{
			report.Identifier{
				Type:  "brakeman_warning_code",
				Name:  "Brakeman Warning Code 13",
				Value: "13",
			},
			report.CWEIdentifier(913),
			report.CWEIdentifier(95),
		},
	}
	for i, w := range warnings {
		got := w.Identifiers()

		if !reflect.DeepEqual(want[i], got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	}
}
