# Brakeman analyzer changelog

## v4.1.2
- Hardcode the secure report version to `15.0.7` (!152)

## v4.1.1
- upgrade `github.com/urfave/cli/v2` version [`v2.27.1` => [`v2.27.2`](https://github.com/urfave/cli/releases/tag/v2.27.2)] (!151)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.2.0` => [`v2.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.4.0)] (!151)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.2` => [`v4.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.4.0)] (!151)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.8` => [`v2.0.9`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.9)] (!151)

## v4.1.0
- include CWEs in returned identifiers (!129)

## v4.0.8
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.7` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!147)

## v4.0.7
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!146)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!146)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.7`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.7)] (!146)

## v4.0.6
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!144)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!144)
- upgrade `github.com/go-git/go-git/v5` version [`v5.4.2` => [`v5.11.0`](https://github.com/go-git/go-git/releases/tag/v5.11.0)] (!144)

## v4.0.5
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (!141)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!141)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (!141)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!141)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!141)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!141)

## v4.0.4
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.4.1`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.4.1) (!139)

## v4.0.3
- upgrade `gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator` to [`v2.3.8`](https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator/-/releases/v2.3.8) (!137)

## v4.0.2
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!124)

## v4.0.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v1.4.1` => [`v2.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.2)] (!125)

## v4.0.0
- Bump to next major version (!120)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!120)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!120)

## v3.1.10
- upgrade `github.com/urfave/cli/v2` version [`v2.25.0` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!121)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!121)

## v3.1.9
- upgrade [`Brakeman`](https://github.com/presidentbeef/brakeman) version [`5.4.0` => [`5.4.1`](https://github.com/presidentbeef/brakeman/releases/tag/v5.4.1)] (!119)
- upgrade `github.com/urfave/cli/v2` version [`v2.24.3` => [`v2.25.0`](https://github.com/urfave/cli/releases/tag/v2.25.0)] (!119)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!119)
- update Go version to 1.19

## v3.1.8
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.24.3`](https://github.com/urfave/cli/releases/tag/v2.24.3)] (!118)

## v3.1.7
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!117)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!117)

## v3.1.6
- upgrade [`Brakeman`](https://github.com/presidentbeef/brakeman) version [`5.3.1` => [`5.4.0`](https://github.com/presidentbeef/brakeman/releases/tag/v5.4.0)] (!115)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!115)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!115)

## v3.1.5
- Add `VET_LANG_EXT` env var to run VET against ruby files (!112)

## v3.1.4
- upgrade [`Brakeman`](https://github.com/presidentbeef/brakeman) version [`5.2.2` => [`5.3.1`](https://github.com/presidentbeef/brakeman/releases/tag/v5.3.1)] (!113)
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!113)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!113)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.0` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!113)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!113)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.5` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!113)

## v3.1.3
- Upgrade gitlab.com/gitlab-org/security-products/analyzers/report to v3.15.5 (!111)

## v3.1.2
- Update common to `v3.2.1` to fix gotestsum cmd (!109)

## v3.1.1
- Upgrade VET version with fixed nil pointer dereference (!107)

## v3.1.0
- Upgrade core analyzer dependencies (!105)
  + Adds support for globstar patterns when excluding paths

## v3.0.1
- Upgrade VET version to 0.15.0 with a new configuration format (!103)

## v3.0.0
- Bump to next major version `v3.0.0` (!102)

## v2.23.1
- Update brakeman to [v5.2.2](https://github.com/presidentbeef/brakeman/releases/tag/v5.2.2) (!100)
  - Respect equality in if conditions
  - Update message for unsafe reflection
  - Handle nil when joining values
  - Add additional String methods for SQL injection check
  - Update ruby_parser for Ruby 3.1 support

## v2.23.0
- Update brakeman to [v5.2.1](https://github.com/presidentbeef/brakeman/releases/tag/v5.2.1) (!98)
  - Add warning codes for EOL software warnings
- Also includes changes in [v5.2.0](https://github.com/presidentbeef/brakeman/releases/tag/v5.2.0) (!98)
  - Initial Rails 7 support
  - Fix issue with calls to foo.root in routes
  - Ignore I18n.locale in SQL queries
  - Do not treat sanitize_sql_like as safe
  - Add new checks for unsupported Ruby and Rails version
- Update tracking calculator (!96)

## v2.22.0
- Update ruleset, report, and command modules to support ruleset overrides (!94)

## v2.21.3
- Bump vet version for improved performance (!93)

## v2.21.2
- Bump vet and post-analyzer/scripts version for more consistent error handling (!92)

## v2.21.1
- Upgrade to to v1.17 (!91)

## v2.21.0
- Update brakeman to [v5.1.2](https://brakemanscanner.org/blog/2021/10/28/brakeman-5-dot-1-dot-2-released) (!90)
  - Updated ruby_parser
  - Fix issue where the previous output is still visible
  - Handle cases where enums are not symbols
  - Support newer Haml with ::Haml::AttributeBuilder.build
  - Fix sorting with nil line numbers

## v2.20.3
- Bumped report module to latest, updating schema to v14.0.4 (!89)

## v2.20.2
- Bumped scripts to v0.0.4 (!88)

## v2.20.1
- Bumped VET version to v0.8.3; previous versions could crash under certain conditions (!87)

## v2.20.0
- Bumped stencils version (!86)

## v2.19.0
- Update brakeman to [v5.1.1](https://brakemanscanner.org/blog/2021/07/19/brakeman-5-dot-1-dot-0-released) (!85)
  - Enhanced performance: read and parse files in parallel
  - Additional Ruby interpretation support for ActiveRecord enums,
    Array, and Has
  - Updated SQL injection support
  - Various other fixes and refactorings

## v2.18.3
- update tracking calculator (!84)

## v2.18.2
- Fix bug which effectively disabled tracking-calculator by updating VET version (!81)

## v2.18.1
- Bump VET version to remove default flag array in gl-sast reports (!80)

## v2.18.0
- Add false-positive scanning and flagging to the analyzer via VET (!79)

## v2.17.1
- Update tracking calculator (!77)

## v2.17.0
- Update brakeman to [v5.0.1](https://brakemanscanner.org/blog/2021/04/27/brakeman-5-dot-0-dot-1-released) (!69)
  - Small update with bugfixes

## v2.16.1
- Update tracking-calculator to v2.0.3 (!64)
  - Improve performance for files containing multiple findings
  - Fix indentation regression to 2-space formatting

## v2.16.0
- Update tracking-calculator  to v2, renames fingerprints to signatures (!62)

## v2.15.0
- Update report dependency in order to use the report schema version
  14.0.0 (!61)

## v2.14.0
- Integrate tracking-calculator post-analyzer with the `vulnerability_finding_fingerprints` feature flag (!56)

## v2.13.0
- Update brakeman to [v5.0.0](https://brakemanscanner.org/blog/2021/01/26/brakeman-5-dot-0-dot-0-released) (!55)
- Now scanning almost all Ruby files in a project that has a Gemfile or
  any *.rb file (!55)

## v2.12.2
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!54)

## v2.12.1
- Update webrick gem to v1.6.1 (!53)
- Update musl, musl-utils to v1.1.24-r10 (!53)
- Update ca-certificates-bundle to 20101127-4 (!53)
- Update ssl_client to 1.31.1-r19 (!53)
- Update alpine-baselayout to 3.2.0-r7 (!53)

## v2.12.0
- Update common to v2.22.0 (!52)
- Update urfave/cli to v2.3.0 (!52)

## v2.11.0
- Update common and enabled disablement of rulesets (!50)

## v2.10.1
- Reclassify confidence level as severity (!49)

## v2.10.0
- Update brakeman scanner to v4.10.0 (!48)
- Update golang dependencies

## v2.9.2
- Update `metadata.ScannerVersion` to match brakeman version `4.9.1` (!43)

## v2.9.1
- Update golang dependencies (!42)

## v2.9.0
- Update brakeman to [v4.9.1](https://brakemanscanner.org/blog/2020/09/04/brakeman-4-dot-9-dot-1-released) (!40)
- Update golang dependencies (!40)

## v2.8.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!39)

## v2.7.0
- Switch to Alpine Linux docker container (!38)
- Update golang to v1.15

## v2.6.0
- Add scan object to report (!34)

## v2.5.0
- Bump brakeman to v4.9.0 (!33)

## v2.4.1
- Add `app.Version` to support `scan.scanner.version` field in security reports (!32)

## v2.4.0
- Switch to the MIT Expat license (!28)

## v2.3.1
- Update Debug output to give a better description of command that was ran (!30)

## v2.3.0
- Update logging to be standardized across analyzers (!29)

## v2.2.1
- Use `ruby:2.7-slim` (Debian Buster Slim) as a base Docker image (!27)
- Remove `location.dependency` from the generated SAST report (!26)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!21)

## v2.1.0
- Add support for custom CA certs (!18)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Bump brakeman to 4.3.1
- Don't detect a Rails application unless "rails" is a first level dependency
- Shows brakeman command error output
- Don't ignore Brakeman exit code

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
