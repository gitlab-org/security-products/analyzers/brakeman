package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "brakeman",
		Name:    "Brakeman",
		Version: metadata.ScannerVersion,
		Vendor:  metadata.ReportScanner.Vendor,
		URL:     "https://brakemanscanner.org",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
